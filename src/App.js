import React, { Component } from 'react';
import { 
  BrowserRouter as Router,
  Route,
} from'react-router-dom';
import './App.css';


import NavApp from'./components/nav';
import FooterApp from'./components/footer';

import Home from'./pages/home';
import Tasks from'./pages/tasks';
import Users from'./pages/users';

class App extends Component {
  render() {
    return (
      <Router>
        <div className="container">
          <NavApp/>
          <div className="div-body">
            <Route exact path="/" component={Home} />
            <Route exact path="/tasks" component={Tasks} />
            <Route exact path="/users" component={Users} />
          </div>
          <FooterApp/>
        </div>
      </Router>
    );
  }
}

export default App;
