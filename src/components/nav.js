import React from'react';
import { Link } from'react-router-dom';
import { Navbar, Nav } from "react-bootstrap";

const NavApp = () => (
  <Navbar fixed="top" bg="dark" variant="dark">
    <Navbar.Brand><Link className="nav-link Link-logo" to="/">Welcome Panel</Link></Navbar.Brand>
    <Nav className="mr-auto Nav-mb-3">
        <Link className="nav-link" to="/">Home</Link>
        <Link className="nav-link" to="/users">Users</Link>
        <Link className="nav-link" to="/tasks">Tasks</Link>
    </Nav>
  </Navbar>
);

export default NavApp;
