import React from'react';
import { Link } from'react-router-dom';
import { Navbar, Nav } from "react-bootstrap";

const FooterApp = () => (
  <Navbar fixed="bottom" bg="light">
    <Nav className="mr-auto Nav-mb-3">
        <Link className="nav-link" to="/">Home</Link>
        <Link className="nav-link" to="/users">Users</Link>
        <Link className="nav-link" to="/tasks">Tasks</Link>
    </Nav>

    <Navbar.Toggle />
    <Navbar.Collapse className="justify-content-end">
        <Navbar.Text>
            Copyright Angelica Simarra 2019
        </Navbar.Text>
    </Navbar.Collapse>
  </Navbar>
);

export default FooterApp;
