import React from 'react';
import { Form, Col, Button } from "react-bootstrap";
import Select from 'react-select';
import store from "../../store";
import { URL_API_USER, URL_API_TASK } from "../../constants/api_url";

class AddTask extends React.Component {
    constructor(props){
        super(props);
        this.state = { usersData: null, users:null, status: 'Open', task: '', id:null }

        store.subscribe( () => {
            let users_items = ( typeof store.getState().users == "string" ) ? JSON.parse(store.getState().users ) : store.getState().users;
            this.setState({
                id: store.getState().id,
                users: users_items,
                status: store.getState().status,
                task: store.getState().task
            });
        });
    }

    componentDidMount() {
        this.searchUsers();
    }

    searchUsers(){
        fetch(URL_API_USER)
        .then(  (response) => {
            return response.json()
        }).then(usersData => {
            this.setState({
                usersData
            });
        })
    }

    handleClearFormClick =  () => {
        store.dispatch({
            type: "CLEAR_FORM_TASK"
        })
    }

    handleChange = (users) => {
        this.setState({ users: users });
    }

    handleChangeInputs (event) {
        this.setState({ [event.target.name]: event.target.value  });
    }

    renderOptionUser =  (dataUsers) => {
        let optionsUser = [];
        let dtuser = (dataUsers == "string") ? JSON.parse(dataUsers) : dataUsers;
        dtuser.map( user => optionsUser.push({ value: user.id, label: user.name }) );
        return <Select  
                value={this.state.users} 
                onChange={ this.handleChange }
                options={optionsUser} 
                isMulti={true}/>
    }

    render() {
        return (
            <div>
                <Form.Row>
                    <Form.Group sm="3"  as={Col} controlId="task">
                        <Form.Label>Task</Form.Label>
                        <Form.Control value={this.state.task} onChange={event => this.handleChangeInputs(event)} name="task" type="text" placeholder="Enter Task" />
                    </Form.Group>

                    <Form.Group sm="5" as={Col} controlId="formResponse">
                        <Form.Label>Users</Form.Label>
                        {this.state.usersData ?
                            this.renderOptionUser(this.state.usersData) :
                            null
                        }
                    </Form.Group>                

                    <Form.Group sm="2" as={Col} controlId="status">
                        <Form.Label>Status</Form.Label>
                        <Form.Control value={this.state.status}  onChange={event => this.handleChangeInputs(event)} name="status" as="select">
                            <option value="Open">Open</option>
                            <option value="In-Progress">In-Progress</option>
                            <option value="Completed">Completed</option>
                            <option value="Archived">Archived</option>
                        </Form.Control>
                    </Form.Group>

                    <Form.Group as={Col} controlId="formBtn">
                        <Form.Label>&nbsp;</Form.Label>
                        <br />
                        <Button variant="primary" onClick={ () => this.handleAddUpdateTaskClick(this.state)  }>
                            Save
                        </Button>
                            &nbsp; 
                        <Button variant="danger" onClick={ this.handleClearFormClick  }>
                            Clear
                        </Button>
                    </Form.Group>

                </Form.Row>
            </div>
        )
    }

    handleAddUpdateTaskClick(task){
        if(parseInt(task.id) > 0){
            this.handleUpdateTask(task);
        }else{
            this.handleAddTask(task);
        }
    }

    handleUpdateTask(task){
        console.log( typeof task.users );
        
        fetch(URL_API_TASK+"/"+task.id, {
            method: 'PUT',
            body: JSON.stringify(task),
            headers: { 'Content-Type': 'application/json' }
        })
        .then((response) => {
            return response.json()
        }).then(respData => {
            if (respData.error !== true) {
                store.dispatch({
                    type: "UPDATE_TASK",
                    task
                })
            } else {
                alert("Error: " + respData.message);
            }
        })
    }

    handleAddTask(task){
        fetch(URL_API_TASK, {
            method: 'POST',
            body: JSON.stringify(task),
            headers: { 'Content-Type': 'application/json' }
        })
        .then((response) => {
            return response.json()
        }).then(respData => {
            if (respData.error !== true) {
                task.id = respData;
                store.dispatch({
                    type: "ADD_TO_TASK",
                    task
                })
            } else {
                alert("Error: " + respData.message);
            }
        })
    }
}

export default AddTask;
