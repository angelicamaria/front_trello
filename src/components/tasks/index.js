import React from'react';
import { Table, Button, Badge } from "react-bootstrap";
import AddTask from "./AddTask";
import { URL_API_TASK } from "../../constants/api_url";
import store from "../../store";

class TableTask extends React.Component {
    constructor(props) {
        super(props);
        this.state = { 
            taskData: null
        };

        store.subscribe( () => {
            this.setState({
                taskData: store.getState().tasks
            });
        });
    }

    componentDidMount() {
        this.searchTasks();
    }

    searchTasks(){
        fetch(URL_API_TASK)
        .then(  (response) => {
            return response.json()
        }).then(taskData => {
            store.dispatch({
                type: "FETCH_TASK",
                tasks: taskData
            })
        })
    }

    editTask(task){
        delete task["created_at"];
        store.dispatch({
            type: "EDIT_TASK",
            task: task
        })
    }

    deleteTask(id) {
        let config = {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json'
            }
        }

        fetch(URL_API_TASK + `/${id}`, config)
            .then((response) => {
                return response.json()
            }).then(taskDataResp => {
                if (taskDataResp.error !== true) {
                    store.dispatch({
                        type: "DELETE_TASK",
                        id
                    })
                } else {
                    alert("Error: " + taskDataResp.message);
                }
            })
    }

    formatUsers(usersItem){
        if( typeof usersItem != null ){
            let users_items = (typeof usersItem == "string") ? JSON.parse(usersItem) : usersItem;
            return users_items.map( (item, index) => <Badge className="badge" key={`badge_${index}`} pill variant="primary"> { item.label } </Badge>)
        }
    }

    renderTask(taskData){
        //console.log(taskData);
        return taskData.map((task, index) => (
            <tr key={index+ 1}>
                <td>{index+1}</td>
                <td>{task.task}</td>
                <td>{ this.formatUsers(task.users) }</td>
                <td>{task.status}</td>
                <td align="center" width="15%">
                    <Button variant="outline-primary" onClick={ () => this.editTask(task) }>Edit Task</Button>
                </td>
                <td align="center" width="15%">
                    <Button variant="outline-danger" onClick={ () => this.deleteTask(task.id) }>Delete Task</Button>
                </td>
            </tr>
        ));
    }

    renderProgress(){
        return <tr><td colSpan='6'>Wait, loading task....</td></tr>;
    }

    render() {
        const { taskData } = this.state;
        return (
            <div className="Table-App">
                <AddTask />

                <Table responsive striped bordered hover size="sm">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Task</th>
                            <th>Users</th>
                            <th>State</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        {taskData ?
                            this.renderTask(taskData) :
                            this.renderProgress()
                        }
                    </tbody>
                </Table>
            </div>
        )
    }
}

export default TableTask;

