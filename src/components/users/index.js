import React from'react';
import { Table, Button } from "react-bootstrap";
import AddUser from "./AddUser";
import { URL_API_USER } from "../../constants/api_url";

class TableUsers extends React.Component {
    constructor(props) {
        super(props);
        this.state = { userData: null, action: "ADD" };
    }

    componentDidMount() {
        this.searchUsers();
    }

    searchUsers(){
        fetch(URL_API_USER)
        .then(  (response) => {
            return response.json()
        }).then(userData => {
            this.setState({
                userData
            });
        })
    }

    onClearForm() {
        document.getElementById("formId").value = "";
        document.getElementById("formName").value = "";
        document.getElementById("formProfile").value = "user";
    }

    switchAddUpdateUser(user) {
        let id_user = document.getElementById("formId").value;
        if( parseInt(id_user) > 0 ){
            //UPDATE
            
            let user = {
                name:    document.getElementById("formName").value,
                profile: document.getElementById("formProfile").value
            }
            let config = {
                method: 'PUT',
                body: JSON.stringify(user),
                headers: {
                    'Content-Type': 'application/json'
                }
            }
            
            fetch(URL_API_USER+"/"+id_user, config)
                .then((response) => {
                    return response.json()
                }).then(respData => {
                    if (respData.error !== true) {
                        this.onClearForm();
                        var userDataState = this.state.userData.filter((user_old) => {
                            return user_old.id != id_user;
                        });
                        const userData = userDataState.concat({ ...user, id: id_user });
                        this.setState({
                            userData
                        });
                    } else {
                        alert("Error: " + respData.message);
                    }
                })
        }else{
            //ADD
            let config = {
                method: 'POST',
                body: JSON.stringify(user),
                headers: {
                    'Content-Type': 'application/json'
                }
            }
    
            fetch(URL_API_USER, config)
                .then((response) => {
                    return response.json()
                }).then(respData => {
                    if (respData.error !== true) {
                        const userData = this.state.userData.concat({ ...user, id: respData });
                        this.setState({
                            userData
                        });
                    } else {
                        alert("Error: " + respData.message);
                    }
                })
        }
    }

    editUser(user){
        this.setState({ action: 'UPDATE' });
        document.getElementById("formId").value = user.id;
        document.getElementById("formName").value = user.name;
        document.getElementById("formProfile").value = user.profile;
    }

    deleteUser(id) {
        let config = {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json'
            }
        }

        fetch(URL_API_USER + `/${id}`, config)
            .then((response) => {
                return response.json()
            }).then(userDataResp => {
                if (userDataResp.error !== true) {
                    var userData = this.state.userData.filter((user) => {
                        return user.id !== id;
                    });
                    this.setState({
                        userData
                    });
                } else {
                    alert("Error: " + userDataResp.message);
                }
            })
    }

    renderUser(userData){
        return userData.map((user, index) => (
            <tr key={index+ 1}>
                <td>{index+1}</td>
                <td>{user.name}</td>
                <td>{user.profile}</td>
                <td align="center" width="15%">
                    <Button onClick={ () => this.editUser(user) } variant="outline-primary">Edit User</Button>
                </td>
                <td align="center" width="15%">
                    <Button variant="outline-danger" onClick={ () => this.deleteUser(user.id) }>Delete User</Button>
                </td>
            </tr>
        ));
    }

    renderProgress(){
        return <tr><td colSpan='5'>Wait, loading users....</td></tr>;
    }

    render() {
        const { userData } = this.state;
        return (
            <div className="Table-App">
                <AddUser onAddUser={(user) => this.switchAddUpdateUser(user)} onClearForm={() => this.onClearForm()}/>

                <Table responsive striped bordered hover size="sm">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>First Name</th>
                            <th>Profile</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        {userData ?
                            this.renderUser(userData) :
                            this.renderProgress()
                        }
                    </tbody>
                </Table>
            </div>
        )
    }
}

export default TableUsers;
