import React from 'react';
import { Form, Col, Button } from "react-bootstrap";

const AddUser = ({ onAddUser, onClearForm }) => {

    const handleAddUserClick =  () => {
        let user = {
            name:       document.getElementById("formName").value,
            profile:    document.getElementById("formProfile").value,
        };
        onAddUser(user);
    }
    
    const handleClearFormClick =  () => {
        onClearForm();
    }

    return (
        <div>
            <Form.Row>
                <Form.Control id="formId" type="hidden"/>

                <Form.Group as={Col} controlId="formName">
                    <Form.Label>Full Name</Form.Label>
                    <Form.Control name="name" type="text" placeholder="Enter Full Name" />
                </Form.Group>

                <Form.Group as={Col} controlId="formProfile">
                    <Form.Label>Profile</Form.Label>
                    <Form.Control name="profile" as="select">
                        <option value="user">User</option>
                        <option value="admin">Admin</option>
                    </Form.Control>
                </Form.Group>

                <Form.Group as={Col} controlId="formBtn">
                    <Form.Label>&nbsp;</Form.Label>
                    <br />
                    <Button variant="primary" onClick={ handleAddUserClick }>
                        Save
                    </Button>
                        &nbsp; 
                    <Button variant="danger" onClick={ handleClearFormClick }>
                        Clear
                    </Button>
                </Form.Group>

            </Form.Row>
        </div>
    );
};

export default AddUser;
