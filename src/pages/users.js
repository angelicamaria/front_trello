import React from'react';

import TableUsers from "../components/users";

const Users = () => (
  <section>
    <div className="Div-content">
      
      <h3>Users</h3>
      <TableUsers />

    </div>
  </section>
);

export default Users;