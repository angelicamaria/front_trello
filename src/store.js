import { createStore } from 'redux';

const reducer = (state, action) => {
    if(action.type === "ADD_TO_TASK"){
        return {
            ...state,
            id: null,
            users: null,
            status: 'Open',
            task: '',
            tasks: state.tasks.concat(action.task)
        }
    }else if(action.type === "FETCH_TASK"){
        return {
            ...state,
            id: null,
            tasks: action.tasks
        }
    }else if(action.type === "EDIT_TASK"){
        return {
            ...state,
            id: action.task.id,
            users: action.task.users,
            status: action.task.status,
            task: action.task.task
        }
    }else if(action.type === "UPDATE_TASK"){
        var taskDataState = state.tasks.filter((task_old) => {
            return task_old.id !== action.task.id;
        });
        const taskData = taskDataState.concat({ ...action.task });
        return {
            ...state,
            id: null,
            users: null,
            status: 'Open',
            task: '',
            tasks: taskData
        }
    }
    else if(action.type === "DELETE_TASK"){
        var taskDataState = state.tasks.filter((task_old) => {
            return task_old.id !== action.id;
        });
        return {
            ...state,
            tasks: taskDataState
        }
    }else if(action.type === "CLEAR_FORM_TASK"){
        return {
            ...state,
            id: null,
            users: null,
            status: 'Open',
            task: '',
        }
    }
    return state;
}

export default createStore(reducer, { id: null, users: null, status: 'Open', task: '', tasks: null } );